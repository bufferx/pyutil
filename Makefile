all:
	@echo "do nothing"

.PHONY : clean
clean:
	cd ./lib/ && make clean
	rm -f *.pyc
